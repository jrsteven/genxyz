//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr  8 08:47:31 2015 by ROOT version 5.34/09
// from TTree T/T
// found on file: output.root
//////////////////////////////////////////////////////////

#ifndef Zc3900_h
#define Zc3900_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.
#include <TLorentzVector.h>
#include <TObject.h>
#include <TVector3.h>

// Fixed size dimensions of array or collections stored in the TTree if any.

class Zc3900 : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
 //TLorentzVector  *pProtonInit;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pGammaInit;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pNeutron;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pZc;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pPi;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pPsi;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pEle;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;
 //TLorentzVector  *pPos;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   UInt_t          fP_fUniqueID;
   UInt_t          fP_fBits;
   Double_t        fP_fX;
   Double_t        fP_fY;
   Double_t        fP_fZ;
   Double_t        fE;

   // List of branches
   TBranch        *b_pProtonInit_fUniqueID;   //!
   TBranch        *b_pProtonInit_fBits;   //!
   TBranch        *b_pProtonInit_fP_fUniqueID;   //!
   TBranch        *b_pProtonInit_fP_fBits;   //!
   TBranch        *b_pProtonInit_fP_fX;   //!
   TBranch        *b_pProtonInit_fP_fY;   //!
   TBranch        *b_pProtonInit_fP_fZ;   //!
   TBranch        *b_pProtonInit_fE;   //!
   TBranch        *b_pGammaInit_fUniqueID;   //!
   TBranch        *b_pGammaInit_fBits;   //!
   TBranch        *b_pGammaInit_fP_fUniqueID;   //!
   TBranch        *b_pGammaInit_fP_fBits;   //!
   TBranch        *b_pGammaInit_fP_fX;   //!
   TBranch        *b_pGammaInit_fP_fY;   //!
   TBranch        *b_pGammaInit_fP_fZ;   //!
   TBranch        *b_pGammaInit_fE;   //!
   TBranch        *b_pNeutron_fUniqueID;   //!
   TBranch        *b_pNeutron_fBits;   //!
   TBranch        *b_pNeutron_fP_fUniqueID;   //!
   TBranch        *b_pNeutron_fP_fBits;   //!
   TBranch        *b_pNeutron_fP_fX;   //!
   TBranch        *b_pNeutron_fP_fY;   //!
   TBranch        *b_pNeutron_fP_fZ;   //!
   TBranch        *b_pNeutron_fE;   //!
   TBranch        *b_pZc_fUniqueID;   //!
   TBranch        *b_pZc_fBits;   //!
   TBranch        *b_pZc_fP_fUniqueID;   //!
   TBranch        *b_pZc_fP_fBits;   //!
   TBranch        *b_pZc_fP_fX;   //!
   TBranch        *b_pZc_fP_fY;   //!
   TBranch        *b_pZc_fP_fZ;   //!
   TBranch        *b_pZc_fE;   //!
   TBranch        *b_pPi_fUniqueID;   //!
   TBranch        *b_pPi_fBits;   //!
   TBranch        *b_pPi_fP_fUniqueID;   //!
   TBranch        *b_pPi_fP_fBits;   //!
   TBranch        *b_pPi_fP_fX;   //!
   TBranch        *b_pPi_fP_fY;   //!
   TBranch        *b_pPi_fP_fZ;   //!
   TBranch        *b_pPi_fE;   //!
   TBranch        *b_pPsi_fUniqueID;   //!
   TBranch        *b_pPsi_fBits;   //!
   TBranch        *b_pPsi_fP_fUniqueID;   //!
   TBranch        *b_pPsi_fP_fBits;   //!
   TBranch        *b_pPsi_fP_fX;   //!
   TBranch        *b_pPsi_fP_fY;   //!
   TBranch        *b_pPsi_fP_fZ;   //!
   TBranch        *b_pPsi_fE;   //!
   TBranch        *b_pEle_fUniqueID;   //!
   TBranch        *b_pEle_fBits;   //!
   TBranch        *b_pEle_fP_fUniqueID;   //!
   TBranch        *b_pEle_fP_fBits;   //!
   TBranch        *b_pEle_fP_fX;   //!
   TBranch        *b_pEle_fP_fY;   //!
   TBranch        *b_pEle_fP_fZ;   //!
   TBranch        *b_pEle_fE;   //!
   TBranch        *b_pPos_fUniqueID;   //!
   TBranch        *b_pPos_fBits;   //!
   TBranch        *b_pPos_fP_fUniqueID;   //!
   TBranch        *b_pPos_fP_fBits;   //!
   TBranch        *b_pPos_fP_fX;   //!
   TBranch        *b_pPos_fP_fY;   //!
   TBranch        *b_pPos_fP_fZ;   //!
   TBranch        *b_pPos_fE;   //!

   Zc3900(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~Zc3900() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(Zc3900,0);
};

#endif

#ifdef Zc3900_cxx
void Zc3900::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pProtonInit_fUniqueID);
   fChain->SetBranchAddress("fBits", &fBits, &b_pProtonInit_fBits);
   fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pProtonInit_fP_fUniqueID);
   fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pProtonInit_fP_fBits);
   fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pProtonInit_fP_fX);
   fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pProtonInit_fP_fY);
   fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pProtonInit_fP_fZ);
   fChain->SetBranchAddress("fE", &fE, &b_pProtonInit_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pGammaInit_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pGammaInit_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pGammaInit_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pGammaInit_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pGammaInit_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pGammaInit_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pGammaInit_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pGammaInit_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pNeutron_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pNeutron_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pNeutron_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pNeutron_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pNeutron_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pNeutron_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pNeutron_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pNeutron_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pZc_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pZc_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pZc_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pZc_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pZc_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pZc_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pZc_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pZc_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pPi_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pPi_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pPi_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pPi_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pPi_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pPi_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pPi_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pPi_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pPsi_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pPsi_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pPsi_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pPsi_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pPsi_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pPsi_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pPsi_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pPsi_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pEle_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pEle_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pEle_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pEle_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pEle_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pEle_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pEle_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pEle_fE);
//    fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_pPos_fUniqueID);
//    fChain->SetBranchAddress("fBits", &fBits, &b_pPos_fBits);
//    fChain->SetBranchAddress("fP.fUniqueID", &fP_fUniqueID, &b_pPos_fP_fUniqueID);
//    fChain->SetBranchAddress("fP.fBits", &fP_fBits, &b_pPos_fP_fBits);
//    fChain->SetBranchAddress("fP.fX", &fP_fX, &b_pPos_fP_fX);
//    fChain->SetBranchAddress("fP.fY", &fP_fY, &b_pPos_fP_fY);
//    fChain->SetBranchAddress("fP.fZ", &fP_fZ, &b_pPos_fP_fZ);
//    fChain->SetBranchAddress("fE", &fE, &b_pPos_fE);
}

Bool_t Zc3900::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef Zc3900_cxx
