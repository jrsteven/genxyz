An event generator for photoproduction of exotic Zc(3900) meson based on the predictions in arXiv:1308.6345.  This will be extended to other channels in the future.  Requires ROOT installed and correct environment setup.

Steps to generate events:
1) Create the beamPhoton.root file by running the photonFlux.C macro, where you can modify the electron and proton beam energies and maximum Q^2 for photoproduction.

root -b -q photonFlux.C

2) Run genZc.C with the following command

root -b -q genZc.C+

Inside genZc.C you can modify the number of events you want to generate.

For questions, contact Justin Stevens: jrsteven@jlab.org